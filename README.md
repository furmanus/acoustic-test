# React + TypeScript + Vite

- run `git clone https://gitlab.com/docent_furman/acoustic-test.git`
- run `npm install`
- run `npm run dev` or optionally `npm run build` to generate production bundle with html file

Initial project code generated with Vite.js https://vitejs.dev/
