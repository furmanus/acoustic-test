import type { Theme } from '@mui/material';
export function secondaryColorSx(theme: Theme): { color: string } {
    return { color: theme.palette.text.secondary };
}

export function grayBackgroundSx(theme: Theme): {background: string} {
    return { background: theme.palette.grey.A200 };
}
