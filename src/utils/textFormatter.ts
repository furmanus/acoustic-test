import { parseAttrsStringToProps } from './html';

type UsePreformattedTextReturnType = [
  tagName: string | null,
  props: Record<string, unknown>,
  children: string,
];

const allowedTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'b', 'br', 'span', 'div'];

export function parsePreformattedText(rawContent: string): UsePreformattedTextReturnType {
    const tagRegexp = /^<(?<tag>[a-z0-9]+)\s*(?<attrs>[a-z-]+=".+?")*>(?<content>.+)<\/[a-z0-9]+>$/gm;
    let tag: string | null = null;
    let props: Record<string, unknown> = {};
    let content: string | null = null;

    const regExpMatchArray = tagRegexp.exec(rawContent.replace('\\n', ''));

    if (regExpMatchArray?.groups) {
        const { tag: groupTag, attrs, content: rawGroupContent } = regExpMatchArray.groups;

        if (groupTag && allowedTags.includes(groupTag)) {
            props = parseAttrsStringToProps(attrs || '');
            tag = groupTag;
            content = rawGroupContent ?? '';
        } else {
            content = rawContent ?? '';
        }
    } else {
        content = rawContent;
    }

    return [tag, props, content];
}
