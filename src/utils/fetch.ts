import { HttpCodes, HttpHeadersContentType } from '../constants/http';

export function fetchWrapper<DataType = unknown>(input: RequestInfo, options?: RequestInit): Promise<DataType | void> {
    return fetch(input, options).then(async (response) => {
        const responseType = response.headers.get('Content-Type')?.toLowerCase() || '';
        let data: DataType | undefined;

        if (responseType.includes(HttpHeadersContentType.Json)) {
            data = await response.json();
        } else {
            data = await response.text() as DataType;
        }

        if (response.ok) {
            if (response.status === HttpCodes.NoContent) {
                return;
            }

            return data;
        } else {
            return Promise.reject(data);
        }
    });
}
