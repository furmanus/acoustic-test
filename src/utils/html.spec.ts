import { describe, it, expect } from 'vitest';
import { parseAttrsStringToProps } from './html';

describe('HTML utilities', () => {
    it('should parse string to single prop object', () => {
        expect(parseAttrsStringToProps(' id="id"')).toStrictEqual({
            id: 'id',
        });
    });
    it('should parse string to multi props object', () => {
        expect(parseAttrsStringToProps(' style="display: none; margin-top: 12px;" id="id"')).toStrictEqual({
            style: {
                display: 'none',
                marginTop: '12px',
            },
            id: 'id',
        });
    });
    it('should convert special attributes', () => {
        expect(parseAttrsStringToProps('class="klasa" data-element="element"')).toStrictEqual({
            className: 'klasa',
            'data-element': 'element',
        });
    });
    it('should return empty object for empty string', () => {
        expect(parseAttrsStringToProps('')).toStrictEqual({});
    });
});
