import { describe, expect, it } from 'vitest';
import { parsePreformattedText } from './textFormatter';

describe('Preformatted text parser', () => {
    it('should return unmodified text when no tag is present', () => {
        expect(parsePreformattedText('HALO')).toStrictEqual([null, {}, 'HALO']);
    });
    it('should parse simple string with tag', () => {
        expect(parsePreformattedText('<h2>HALO</h2>')).toStrictEqual(['h2', {}, 'HALO']);
    });
    it('should parse simple string with tag and attrs', () => {
        expect(parsePreformattedText('<p id="a">HALO</p>')).toStrictEqual(['p', { id: 'a' }, 'HALO']);
    });
    it('should parse text with nested tags', () => {
        expect(parsePreformattedText('<h1><span>HALO</span></h1>')).toStrictEqual(['h1', {}, '<span>HALO</span>']);
    });
    it('should sanitize dangerous html', () => {
        expect(parsePreformattedText('<script>alert(1)</script>')).toStrictEqual([null, {}, '<script>alert(1)</script>']);
    });
});
