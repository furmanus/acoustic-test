type LogMethod = 'log' | 'info' | 'warn' | 'error' | 'debug';

class LoggerService {

    #shouldLog = process.env.NODE_ENV === 'development';

    public log(...args: any[]): void {
        this.#log('log', ...args);
    }

    public error(...args: any[]): void {
        this.#log('error', ...args);
    }

    #log(method: LogMethod, ...args: any[]): void {
        if (this.#shouldLog) {
            // eslint-disable-next-line no-console
            console[method](...args);
        }
    }

}

export const loggerService = new LoggerService();
