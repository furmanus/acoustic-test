import { CSSProperties } from 'react';

const nameToReactName: Record<string, string> = {
    class: 'className',
    for: 'htmlFor',
};

export function parseAttrsStringToProps(str: string): Record<string, unknown> {
    const splitStr = str.trim().split('" ').map((examinedString) => examinedString.replace(/"/g, ''));

    return splitStr.reduce((result, current) => {
        const [name, value] = current.split('=');

        if (name && value) {
            if (name === 'style') {
                result.style = parseStyleStrictToCssProperties(value.trim());
            } else {
                result[nameToReactName[name] ?? name] = value;
            }
        }

        return result;
    }, {} as Record<string, unknown>);
}

function parseStyleStrictToCssProperties(text: string): CSSProperties {
    return text.split(';').reduce((result, current) => {
        if (current) {
            const [cssProp, cssValue] = current.trim().split(':');
            // TODO fix typing
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            result[convertDashToCamelCase(cssProp)] = cssValue.trim();
        }

        return result;
    }, {} as CSSProperties);
}

function convertDashToCamelCase(str: string): string {
    const regexp = /-(?<letter>[a-z])/;
    const regExpMatchArray = regexp.exec(str);

    if (regExpMatchArray?.groups?.letter) {
        return str.replace(`-${regExpMatchArray?.groups?.letter}`, regExpMatchArray?.groups?.letter.toUpperCase());
    }

    return str;
}
