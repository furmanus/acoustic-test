import './App.css';
import { Article } from './components/Article';
import { CircularProgress, Paper } from '@mui/material';
import { useArticleData } from './hooks/useArticleData';
import { AppHeading } from './components/AppHeading';
import { Error } from './components/Error';
import { loggerService } from './utils/logger.service';
import { grayBackgroundSx } from './utils/theme';

export function App(): JSX.Element {
    const { data, error, isLoading } = useArticleData();

    if (isLoading || (!data && !error)) {
        return <CircularProgress/>;
    }

    if (error) {
        loggerService.error(error);
        /* eslint-disable max-len */
        return (
            <Paper sx={grayBackgroundSx} component="main" className="pagePaper" elevation={3}>
                <Error
                    message="I'm sorry, but I was unable to get required data for you :( Please try again later."
                />
            </Paper>
        /* eslint-enable max-len */
        );
    }

    const { elements: { author, body, date, heading, mainImage } } = data;

    return (
        <Paper sx={grayBackgroundSx} component="main" className="pagePaper" elevation={3}>
            <AppHeading author={author.value} content={heading.value}/>
            <Article date={date.value} imageUrl={mainImage.value.leadImage.url} body={body.values}/>
        </Paper>
    );
}

