export enum HttpCodes {
    NoContent = 204,
}

export enum HttpHeadersContentType {
    Json = 'application/json',
}
