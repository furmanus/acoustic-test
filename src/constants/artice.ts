export enum ArticleElementType {
    Text = 'text',
    FormattedText = 'formattedtext',
    DateTime = 'datetime',
    Group = 'group',
    Image = 'image',
}

export const HAROLD_ERROR_IMAGE_URL = 'https://upload.wikimedia.org/wikipedia/en/a/a4/Hide_the_Pain_Harold_%28Andr%C3%A1s_Arat%C3%B3%29.jpg';
export const ACOUSTIC_BASE_URL = 'https://content-eu-4.content-cms.com/';
export const ARTICLE_URL = new URL(
    'api/859f2008-a40a-4b92-afd0-24bb44d10124/delivery/v1/content/db4930e9-7504-4d9d-ae6c-33facca754d1',
    ACOUSTIC_BASE_URL,
).href;
