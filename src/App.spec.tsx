import { describe, it, expect } from 'vitest';
import { render, screen, waitFor } from '@testing-library/react';
import { App } from './App.tsx';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { ARTICLE_URL } from './constants/artice.ts';
import mockResponse from '../tests/mockResponse.json';

describe('App component', () => {
    const server = setupServer(
        rest.get(ARTICLE_URL, (_, res, ctx) => res(ctx.json(mockResponse))),
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    it('should render component', async () => {
        render(<App/>);

        await waitFor(() => expect(screen.getByTestId('article')).toBeInTheDocument());
    });
});
