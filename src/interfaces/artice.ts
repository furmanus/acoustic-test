import { ArticleElementType } from '../constants/artice';

type ArticleElementText = {
    elementType: ArticleElementType.Text;
    value: string;
};
type ArticleElementFormattedText = {
    elementType: ArticleElementType.FormattedText;
    values: string[];
};
type ArticleElementDateTime = {
    elementType: ArticleElementType.DateTime;
    value: string;
};
type ArticleElementImage = {
    elementType: ArticleElementType.Image;
    url: string;
};

export type ArticleData = {
    elements: {
        heading: ArticleElementText;
        author: ArticleElementText;
        body: ArticleElementFormattedText;
        date: ArticleElementDateTime;
        mainImage: {
            elementType: ArticleElementType.Group;
            value: {
                leadImage: ArticleElementImage;
                leadImageCaption: ArticleElementText;
                leadImageCredit: {
                    elementType: ArticleElementType.Text;
                };
            };
        };
    };
};
