import { ArticleData } from '../interfaces/artice';
import { useEffect, useState } from 'react';
import { fetchWrapper } from '../utils/fetch';
import { ARTICLE_URL } from '../constants/artice';

type UseArticleHookReturnType = {
    data: null;
    isLoading: false;
    error: Error;
} | {
    data: ArticleData;
    isLoading: false;
    error: null;
} | {
    isLoading: true;
    data: null;
    error: null;
};

export function useArticleData(): UseArticleHookReturnType {
    const [data, setData] = useState<ArticleData | null>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<null | Error>(null);

    useEffect(() => {
        const abortController = new AbortController();

        fetchWrapper<ArticleData>(ARTICLE_URL, { signal: abortController.signal })
            .then((articleData) => {
                if (articleData) {
                    setData(articleData);
                }
            })
            .catch((e: unknown) => {
                if (!(e instanceof DOMException)) {
                    if (e instanceof Error) {
                        setError(e);
                    } else {
                        setError(new Error(e?.toString?.() ?? 'Unknown error'));
                    }
                }
            })
            .finally(() => {
                setIsLoading(false);
            });

        return () => {
            abortController.abort();
        };
    }, []);

    return {
        data,
        isLoading,
        error,
    } as UseArticleHookReturnType;
}
