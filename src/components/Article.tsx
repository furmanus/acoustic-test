import { Card, CardMedia, Typography } from '@mui/material';
import './Article.css';
import { ACOUSTIC_BASE_URL } from '../constants/artice';
import { Preformatted } from './Preformatted';
import { secondaryColorSx } from '../utils/theme';
import { AppearTransition } from './AppearTransition';

interface Props {
    date: string;
    imageUrl: string;
    body: string[];
}

export function Article({ imageUrl, date, body }: Props): JSX.Element {
    return (
        <Card data-testid="article" className="article-wrapper" component="article" elevation={2}>
            <AppearTransition className="article-inner-wrapper">
                {body.map((content) => <Preformatted textContent={content} key={content}/>)}
                <CardMedia className="article-image" image={new URL(imageUrl, ACOUSTIC_BASE_URL).href}/>
                <Typography textAlign="center" sx={secondaryColorSx}>{new Date(date).toDateString()}</Typography>
            </AppearTransition>
        </Card>
    );
}
