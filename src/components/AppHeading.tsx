import { Box, Typography } from '@mui/material';
import { secondaryColorSx } from '../utils/theme';

interface Props {
    author: string;
    content: string;
}

export function AppHeading({ content, author }: Props): JSX.Element {
    return (
        <Box component="header" display="flex" flexDirection="column" alignItems="center">
            <Typography variant="h4" component="h1">{content}</Typography>
            <Typography component="p" sx={secondaryColorSx}>By {author}</Typography>
        </Box>
    );
}
