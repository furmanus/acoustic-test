import { describe, it, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import { Preformatted } from './Preformatted';

describe('Preformatted text component', () => {
    it('should render from simple string', () => {
        const textContent = 'HALO';
        const testId='halo';

        render(<Preformatted textContent={`<h2 data-testid="${testId}">${textContent}</h2>`}/>);

        expect(screen.getByTestId(testId)).toHaveTextContent(textContent);
    });
    it('should render from complex string', async () => {
        const textContent = 'HALO';
        const testId='halo';
        const innerTestId = 'halo2';

        render(<Preformatted textContent={`<div data-testid="${testId}"><span data-testid="${innerTestId}">${textContent}</span></div>`}/>);

        const element = await screen.findByTestId(testId);
        const child = await screen.findByTestId(innerTestId);

        expect(element).toContainElement(child);
    });
    it('should render from complex string with attributes', async () => {
        const textContent = 'HALO';
        const testId='halo';
        const innerTestId = 'halo2';

        render(
            /* eslint-disable max-len */
            <Preformatted
                textContent={`<div class="test-class" data-testid="${testId}"><span id="span-id" data-testid="${innerTestId}">${textContent}</span></div>`}
            />,
            /* eslint-enable max-len */
        );

        const element = await screen.findByTestId(testId);
        const child = await screen.findByTestId(innerTestId);

        expect(element).toHaveAttribute('class', 'test-class');
        expect(child).toHaveAttribute('id', 'span-id');
    });
});
