import { createElement, ReactNode, useMemo } from 'react';
import { decode } from 'html-entities';
import { parsePreformattedText } from '../utils/textFormatter';

interface Props {
    textContent: string;
}

export function Preformatted({ textContent }: Props): ReactNode {
    const [tagName, props, children] = useMemo(() => parsePreformattedText(decode(textContent)), [textContent]);

    if (tagName) {
        return createElement(tagName, props, <Preformatted textContent={children}/>);
    }

    return children;
}

