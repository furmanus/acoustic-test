import { PropsWithChildren, useEffect, useState, HTMLAttributes } from 'react';
import './AppearTransition.css';

export function AppearTransition(props: PropsWithChildren<HTMLAttributes<HTMLDivElement>>): JSX.Element {
    const [isVisible, setIsVisible] = useState(false);
    const { children, className, ...restProps } = props;
    const classText = `appear-transition ${isVisible ? 'appear-visible ' : ' '}${className || ''}`;

    useEffect(() => {
        setIsVisible(true);
    }, []);

    return (<div className={classText} {...restProps}>{props.children}</div>);
}
