import { Card, CardMedia, Typography } from '@mui/material';
import { HAROLD_ERROR_IMAGE_URL } from '../constants/artice';

type Props = {
    message: string;
};

export function Error({ message }: Props): JSX.Element {
    return (
        <Card data-testid="article-error" className="article-wrapper article-error" component="article" elevation={2}>
            <CardMedia className="article-image" image={HAROLD_ERROR_IMAGE_URL}/>
            <Typography>{message}</Typography>
        </Card>
    );
}
